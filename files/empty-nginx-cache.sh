#! /bin/bash

# SPDX-FileCopyrightText: 2018 Michael Pöhn <michael@poehn.at>
# SPDX-License-Identifier: GPL-3.0-or-later

systemctl stop nginx && rm -rf /var/cache/nginx/* && systemctl start nginx
