<!--
SPDX-FileCopyrightText: 2018 Michael Pöhn <michael@poehn.at>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# F-Droid Website Search Deployment

## deploy

    ansible-galaxy install -f -r requirements.yml -p .galaxy
    ansible-playbook -i fdroidSearch, webserver.yml
    ansible-playbook -i fdroidSearch, searchcontainer.yml
